class Closure < ApplicationRecord
  class << self
    def valid_geo_ref_types
      [Country, Region].freeze
    end

    def valid_geo_ref_type?(klass)
      valid_geo_ref_types.include? klass
    end

    def kinds
      [
        N_('fixed'),
        N_('flexible'),
        N_('temporary'),
      ].freeze
    end
  end
  has_many :season_closures,
    dependent: :delete_all
  has_many :relevant_season_closures,
    -> { year = Time.now.year ; where year: [(year-1), year] },
    class_name: 'SeasonClosure'

  belongs_to :geo_ref

  has_and_belongs_to_many :geo_refs
  has_and_belongs_to_many :routes

  validates :description, presence: true
  validates :regular_start_month,
    numericality: {
      allow_nil: true,
      greater_than_or_equal_to: 1,
      less_than_or_equal_to: 12
    }
  validates :regular_start_day_of_month,
    numericality: {
      allow_nil: true,
      greater_than_or_equal_to: 1,
      less_than_or_equal_to: 31
    }
  validates :regular_end_month,
    numericality: {
      allow_nil: true,
      greater_than_or_equal_to: 1,
      less_than_or_equal_to: 12
    }
  validates :regular_end_day_of_month,
    numericality: {
      allow_nil: true,
      greater_than_or_equal_to: 1,
      less_than_or_equal_to: 31
    }
  validates :kind,
    inclusion: {
      in: Closure.kinds
    }

  def self.icon
    'lock'
  end

  def ransackable_attributes(auth_object = nil)
    %w[description kind]
  end

  def ransackable_associations(auth_object = nil)
    %w[geo_ref]
  end

  def start_year
    @start_year ||= if multi_year? && !after_end?
                      Time.new.year - 1
                    else
                      Time.new.year
                    end
  end

  def end_year
    @end_year ||= if multi_year? && after_end?
                    Time.new.year + 1
                  else
                    Time.new.year
                  end
  end

  def after_end?
    build_date(Time.new.year, regular_end_month, regular_end_day_of_month) < Date.today
  end

  def multi_year?
    return false if regular_start_month.nil? \
      || regular_end_month.nil? \
      || regular_start_day_of_month.nil? \
      || regular_end_day_of_month.nil?

    build_date(0, regular_start_month, regular_start_day_of_month) > \
      build_date(0, regular_end_month, regular_end_day_of_month)
  end

  def current_season_closure
    @current_season_closure ||= \
      if relevant_season_closures.loaded?
        relevant_season_closures.select { |sc| sc.year == start_year }.first
      else
        season_closures.where(year: start_year).first
      end
  end

  def start_month
    @start_month ||= current_season_closure&.start_month || regular_start_month
  end

  def start_day_of_month
    @start_day_of_month ||= current_season_closure&.start_day_of_month || regular_start_day_of_month
  end

  def end_month
    @end_month ||= current_season_closure&.end_month || regular_end_month
  end

  def end_day_of_month
    @end_day_of_month ||= current_season_closure&.end_day_of_month || regular_end_day_of_month
  end

  def start_at!
    build_date(start_year, start_month, start_day_of_month)
  end

  def regular_start_at
    build_date(start_year, regular_start_month, regular_start_day_of_month)
  end

  def end_at!
    build_date(end_year, end_month, end_day_of_month)
  end

  def regular_end_at
    build_date(end_year, regular_end_month, regular_end_day_of_month)
  end

  def active!
    return false if start_at.nil? || end_at.nil?

    today = Date.today
    today >= start_at && today <= end_at
  end

  def active_changed_at!
    return if end_at!.nil? || start_at!.nil?

    today = Date.today
    [end_at!, start_at!].reject {|d| d > today }.first
  end

  before_save :update_cached_attributes
  def update_cached_attributes
    self.end_at = end_at!
    self.start_at = start_at!
    self.active = active!
    self.active_changed_at = active_changed_at!
  end

  def apply_template(template)
    self.description = _(template.description)
    %i[
      regular_start_month
      regular_start_day_of_month
      regular_end_month
      regular_end_day_of_month
    ].each do |field|
      send("#{field}=", template.send(field))
    end
  end

  protected

  def build_date(year, month, day)
    return if month.nil? || day.nil?

    # use last day of month if day exceeds month days
    last_day_of_month = Date.new(year, month, 1).next_month.prev_day.mday
    Date.new(year, month, day > last_day_of_month ? last_day_of_month : day)
  end
end
