class Topo < ApplicationRecord
  validates_presence_of :title, :target
  belongs_to :target, polymorphic: true

  has_one_attached :background
  validates(
    :background,
    content_type: Routenbuch.supported_image_types,
    size: { less_than: Routenbuch.max_upload_size }
  )

  has_one_attached :overlay
  validates(
    :overlay,
    content_type: [:svg],
    size: { less_than: Routenbuch.max_upload_size }
  )

  def ransackable_attributes(auth_object = nil)
    %w[title]
  end

  def ransackable_associations(auth_object = nil)
    %w[]
  end
end
