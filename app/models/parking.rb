class Parking < GeoRef
  class << self
    def valid_parent_classes
      [Region]
    end
  end

  def supports_ascents?
    false
  end

  def supports_ticklists?
    false
  end

  def supports_paths?
    true
  end
end
