class User < ApplicationRecord
  class << self
    ACCESS_LEVELS = [
      N_('private'),
      N_('internal'),
      *(N_('public') if Routenbuch.public?)
    ].freeze

    def access_levels
      ACCESS_LEVELS
    end
  end

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise_modules = %i[
    database_authenticatable
    recoverable
    rememberable
    trackable
    validatable
  ]
  devise_modules << :registerable if Routenbuch.sign_up?
  devise(*devise_modules)

  validates :access, presence: true, access_level: true
  validates :ascents_access, presence: true, access_level: true
  validates :ticklists_access, presence: true, access_level: true

  has_many :ascents
  has_many :ticklists
  has_many :photos
  has_many :permissions

  ROLES = %w[admin contributor member guest].freeze

  def valid_roles
    ROLES
  end

  validates :password, password_strength: true, unless: Proc.new {|u| u.password.blank?}

  validate :validate_role
  def validate_role
    return if valid_roles.include? role

    errors.add(:role, _('must be one of %{roles}') % { roles: valid_roles.join(', ') } )
  end

  after_initialize :init
  def init
    self.role ||= 'member'
  end

  def self.icon
    'users'
  end

  def icon
    'user'
  end

  def display_name
    if self.name.nil? || self.name.blank?
      "User #{self.id}"
    else
      self.name
    end
  end

  def self.ransackable_attributes(auth_object = nil)
    %w[name role]
  end

  def self.ransackable_associations(auth_object = nil)
    %w[]
  end

  def self.ransackable_scopes(auth_object = nil)
    %i[search_full_text]
  end

  def update_stats
    self.stats = UserPresenters::StatsPresenter.new(self).stats
  end

  def after_ascent_commit(ascent)
    UserUpdateStatsJob.perform_later(id)
  end

  def last_ascent
    ascents.order('ascents.when DESC').first
  end

  has_one_attached :avatar
  validates(
    :avatar,
    content_type: Routenbuch.supported_image_types,
    size: { less_than: Routenbuch.max_upload_size }
  )

  validate :validate_locale
  def validate_locale
    return if locale.blank?
    return if Routenbuch.locales.include?(locale.to_sym)

    errors.add(:locale, _('must be one of: %{values}') % { values: Routenbuch.locales.join(', ') })
  end

  include PgSearch::Model
  pg_search_scope(
    :search_full_text,
    against: {
      name:  'A',
      email: 'A',
    },
    using: {
      tsearch: {
        prefix: true,
        dictionary: 'simple'
      }
    }
  )
end
