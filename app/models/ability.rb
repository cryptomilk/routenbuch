class Ability
  include CanCan::Ability

  def initialize(user)
    alias_action :create, :read, :update, :destroy, to: :crud

    # if no user is present map to guest
    @user = user || User.new( :role => 'guest' )

    if @user.role.present?
      send(@user.role, @user)
    end
  end

  def guest(user)
    can_read_globals

    access_levels = ['public']
    can_geo_ref_access_levels(user, access_levels)
    can_user_access_levels(user, access_levels)
    can_comment_access_levels(user, access_levels)
    can_photo_access_levels(user, access_levels)
  end

  def member(user)
    can_personal_data(user)
    can_read_globals

    access_levels = ['public', 'internal']
    can_geo_ref_access_levels(user, access_levels)
    can_user_access_levels(user, access_levels)
    can_comment_access_levels(user, access_levels)
    can_photo_access_levels(user, access_levels)
  end

  def contributor(user)
    can_personal_data(user)
    can_read_globals

    access_levels = ['public', 'internal']
    can_geo_ref_access_levels(user, access_levels)
    can_user_access_levels(user, access_levels)

    can(
      :create,
      GeoRef,
      parent: {
        access: ['public', 'internal'],
        effective_permissions: {
          level: ['editor', 'coordinator', 'manager'],
          user_id: user.id
        }
      }
    )
    can(
      [:update, :destroy],
      GeoRef,
      access: ['public', 'internal'],
      effective_permissions: {
        level: ['editor', 'coordinator', 'manager'],
        user_id: user.id
      }
    )
    can(
      :create,
      GeoRef,
      parent: {
        access: 'private',
        effective_permissions: {
          level: ['coordinator', 'manager'],
          user_id: user.id
        }
      }
    )
    can(
      [:read, :update, :destroy],
      GeoRef,
      access: 'private',
      effective_permissions: {
        level: ['coordinator', 'manager'],
        user_id: user.id
      }
    )
    can(
      :create,
      GeoRef,
      parent: {
        access: 'confidential',
        effective_permissions: {
          level: ['manager'],
          user_id: user.id
        }
      }
    )
    can(
      [:read, :update, :destroy],
      GeoRef,
      access: 'confidential',
      effective_permissions: {
        level: ['manager'],
        user_id: user.id
      }
    )

    can_crud_by_geo_ref_permissions(user, Route)
    can_crud_by_geo_ref_permissions(user, Inventory, [:route, :geo_ref])
    can_crud_by_geo_ref_permissions(user, RouteLink, [:route_from, :geo_ref])
    can_crud_by_geo_ref_permissions(user, SecondaryZone)
    can_crud_by_geo_ref_permissions(user, Path)
    can_crud_by_geo_ref_permissions(user, Regulation)
    can_crud_by_geo_ref_permissions(user, RegulatedGeoRef, [:regulation, :geo_ref])
    can_crud_by_geo_ref_permissions(user, Closure)

    can_comments_contributor(user)
    can_photos_contributor(user)

    can [:create, :update], FirstAscentPerson
  end

  def admin(user)
    can_personal_data(user)
    can_read_globals
    can_user_access_levels(user, ['public', 'internal'])

    can [:read, :destroy], Comment
    can [:create, :update], Comment, user_id: user.id
    can :crud, Closure
    can :crud, FirstAscentPerson
    can :crud, GeoRef
    can :crud, SecondaryZone
    can :crud, Regulation
    can :crud, RegulatedGeoRef
    can :crud, Route
    can :crud, RouteLink
    can :crud, SeasonClosure
    can :crud, Path
    can [:read, :destroy], Photo
    can [:create, :update], Photo, user_id: user.id
    can :crud, Inventory
    can :read, Product

    can :access, :admin
    can :admin, User
    can :admin, Vendor
    can :admin, Product

    can :crud, Permission
  end

  private

  def can_photo_access_levels(user, levels)
    query = Photo \
      .where(private: false) \
      .with_access(levels)

    can(
      :read,
      Photo,
      query
    ) do |photo|
      levels.include?(photo.target.access) \
        && !photo.private
    end

    if user.role == 'member'
      can [:create, :update], Photo do |photo|
        photo.user_id == user.id \
          && !photo.private \
          && !photo.pinned \
          && !photo.cover_photo \
          && levels.include?(photo.target.access)
      end
      can :destroy, Photo do |photo|
        photo.user_id == user.id \
          && levels.include?(photo.target.access)
      end
    end
  end

  def can_comment_access_levels(user, levels)
    query = Comment \
      .where(private: false) \
      .with_access(levels)

    can(
      :read,
      Comment,
      query
    ) do |comment|
      levels.include?(comment.target.access) \
        && !comment.private
    end

    if user.role == 'member'
      can [:create, :update], Comment do |comment|
        comment.user_id == user.id \
          && !comment.private \
          && !comment.pinned \
          && levels.include?(comment.target.access)
      end
      can :destroy, Comment do |comment|
        comment.user_id == user.id \
          && levels.include?(comment.target.access)
      end
    end
  end

  def can_geo_ref_access_levels(user, levels = [])
    can :read, GeoRef, access: levels
    can :read, Route, geo_ref: { access: levels }
    can :read, Inventory, route: { geo_ref: { access: levels } }
    can :read, RouteLink, route_from: { geo_ref: { access: levels } }
    can :read, SecondaryZone, geo_ref: { access: levels }
    can :read, Path, geo_ref: { access: levels }
    can :read, Regulation, geo_ref: { access: levels }
    can :read, RegulatedGeoRef, geo_ref: { access: levels }
    can :read, Closure, geo_ref: { access: levels }
  end

  def can_user_access_levels(user, levels = [])
    can :read, Ascent, user: { ascents_access: levels }
    can :read, Ticklist, user: { ascents_access: levels }
    can :read, User, access: levels
  end

  def can_read_globals
    can :read, FirstAscentPerson
    can :read, Grade
    can :read, Zone
    can :read, Style
    can :read, Product
    can :read, Vendor
    can :read, :help
    can :check, :password_strength
  end

  def can_personal_data(user)
    can :crud, Ascent, user_id: user.id
    can :crud, Ticklist, user_id: user.id
    can %i[
      read
      update_settings
    ], User, id: user.id
  end

  def nested_path_for(path, content = {})
    tree = content
    path.reverse.each do |p|
      tree = { p => tree }
    end
    tree
  end

  def can_crud_by_geo_ref_permissions(user, object, path = [:geo_ref])
    can(
      [:create, :update, :destroy],
      object,
      nested_path_for(
        path,
        access: ['public', 'internal'],
        effective_permissions: {
          level: ['editor', 'coordinator', 'manager'],
          user_id: user.id
        }
      )
    )
    can(
      :crud,
      object,
      nested_path_for(
        path,
        access: 'private',
        effective_permissions: {
          level: ['coordinator', 'manager'],
          user_id: user.id
        }
      )
    )
    can(
      :crud,
      object,
      nested_path_for(
        path,
        access: 'confidential',
        effective_permissions: {
          level: ['manager'],
          user_id: user.id
        }
      )
    )
  end

  def can_comments_contributor(user)
    query = Comment \
      .where(private: false) \
      .with_access(['public', 'internal']) \
      .or(Comment.with_access(['public', 'internal', 'private'], user, ['coordinator', 'manager'])) \
      .or(Comment.with_access(['confidential'], user, ['manager']))

    can(
      :read,
      Comment,
      query
    ) do |comment|
      case comment.target.access
      when 'public', 'internal'
        if comment.private
          comment.with_permission?(user, 'coordinator', 'manager')
        else
          true
        end
      when 'private'
          comment.with_permission?(user, 'coordinator', 'manager')
      when 'confidential'
          comment.with_permission?(user, 'manager')
      end
    end

    can [:create, :update], Comment do |comment|
      if comment.user_id == user.id
        case comment.target.access
        when 'public', 'internal'
          if comment.private || comment.pinned
            comment.with_permission?(user, 'coordinator', 'manager')
          else
            true
          end
        when 'private'
            comment.with_permission?(user, 'coordinator', 'manager')
        when 'confidential'
            comment.with_permission?(user, 'manager')
        end
      else
        false
      end
    end

    can :destroy, Comment do |comment|
      if comment.user_id == user.id
        true
      else
        case comment.target.access
        when 'public', 'internal', 'private'
            comment.with_permission?(user, 'coordinator', 'manager')
        when 'confidential'
            comment.with_permission?(user, 'manager')
        end
      end
    end
  end

  def can_photos_contributor(user)
    query = Photo \
      .where(private: false) \
      .with_access(['public', 'internal']) \
      .or(Photo.with_access(['public', 'internal', 'private'], user, ['coordinator', 'manager'])) \
      .or(Photo.with_access(['confidential'], user, ['manager']))

    can(
      :read,
      Photo,
      query
    ) do |photo|
      case photo.target.access
      when 'public', 'internal'
        if photo.private
          photo.with_permission?(user, 'coordinator', 'manager')
        else
          true
        end
      when 'private'
          photo.with_permission?(user, 'coordinator', 'manager')
      when 'confidential'
          photo.with_permission?(user, 'manager')
      end
    end

    can [:create, :update], Photo do |photo|
      if photo.user_id == user.id
        case photo.target.access
        when 'public', 'internal'
          if photo.private || photo.pinned || photo.cover_photo
            photo.with_permission?(user, 'coordinator', 'manager')
          else
            true
          end
        when 'private'
            photo.with_permission?(user, 'coordinator', 'manager')
        when 'confidential'
            photo.with_permission?(user, 'manager')
        end
      else
        false
      end
    end

    can :destroy, Photo do |photo|
      if photo.user_id == user.id
        true
      else
        case photo.target.access
        when 'public', 'internal', 'private'
            photo.with_permission?(user, 'coordinator', 'manager')
        when 'confidential'
            photo.with_permission?(user, 'manager')
        end
      end
    end
  end
end
