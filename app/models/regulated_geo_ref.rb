class RegulatedGeoRef < ApplicationRecord
  belongs_to :regulation
  belongs_to :geo_ref
  has_many :inspections

  validate :validate_valid_geo_ref_type
  def validate_valid_geo_ref_type
    return if geo_ref.is_a? Crag

    errors.add(:geo_ref, _('Location must be a crag or sector'))
  end

  validates(
    :geo_ref,
    uniqueness: {
      scope: :regulation,
      message: _('can only be added once')
    }
  )

  def self.icon
    'map-marker'
  end
end
