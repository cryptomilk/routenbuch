class Inspection < ApplicationRecord
  belongs_to :regulated_geo_ref

  def self.ransackable_attributes(_auth_object = nil)
    %w[body]
  end

  def self.ransackable_associations(_auth_object = nil)
    []
  end

  validates :when, presence: true

  has_many_attached :protocols

  include PgSearch::Model
  pg_search_scope(
    :search_full_text,
    against: {
      body: 'A'
    },
    using: {
      tsearch: {
        prefix: true,
        dictionary: 'simple'
      }
    }
  )
end
