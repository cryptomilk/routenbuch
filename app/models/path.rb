class Path < ApplicationRecord
  class << self
    def icon
      'map-o'
    end

    def path_types
      %w[Approach].freeze
    end
  end

  belongs_to :geo_ref
  has_and_belongs_to_many :geo_refs

  validates :name, presence: true

  validates(
    :type,
    inclusion: {
      in: Path.path_types,
      message: '%{value} is not a valid path type!'
    }
  )

  validate :validate_geo_ref
  def validate_geo_ref
    unless geo_ref.type == 'Parking'
      errors.add(:geo_ref, 'Must be of type Parking')
    end
  end

  has_one_attached :track

  include Taggable

  def ransackable_attributes(auth_object = nil)
    %w[name type body]
  end

  def ransackable_associations(auth_object = nil)
    %w[geo_ref geo_refs]
  end
end
