class Product < ApplicationRecord
  class << self
    KINDS = [
      N_('glue-in bolt'),
      N_('expansion bolt'),
      N_('ring'),
      N_('quick link'),
      N_('chain'),
      N_('belay station'),
    ].freeze

    def kinds
      KINDS
    end

    def valid_kind?(kind)
      KINDS.include? kind
    end

    STANDARDS = [
      'EN959:2019 class 1',
      'EN959:2019 class 2',
      'EN959:2019 class 3',
      'EN959:2007',
      'EN12275'
    ].freeze

    def standards
      STANDARDS
    end

    def valid_standard?(standard)
      STANDARDS.include? standard
    end
  end

  belongs_to :vendor, optional: true
  has_many :inventories
  has_many :routes, through: :inventories

  validates :name, presence: true

  validate :validate_kind
  def validate_kind
    return if kind.blank?
    return if self.class.valid_kind? kind

    errors.add(:kind, _('is not a valid kind'))
  end

  validates :url, url: true

  validate :validate_standard
  def validate_standard
    return if standard.blank?
    return if self.class.valid_standard?(standard)

    errors.add(:standard, _('is not a valid standard'))
  end

  has_one_attached :photo
  validates(
    :photo,
    content_type: Routenbuch.supported_image_types,
    size: { less_than: Routenbuch.max_upload_size }
  )

  def self.ransackable_attributes(_auth_object = nil)
    %w[name kind standard]
  end

  def self.ransackable_associations(_auth_object = nil)
    %w[vendor]
  end

  def self.icon
    'shopping-cart'
  end

  def self.grouped_select_collection
    groups = {}
    all.includes(:vendor).each do |p|
      group = p.vendor&.name || _('No vendor')
      groups[group] ||= []
      groups[group] << OpenStruct.new(
        id: p.id,
        label: p.name
      )
    end

    groups.map { |k, v| OpenStruct.new(label: k, items: v ) }.sort_by(&:label)
  end
end
