class AutocompleteInput < SimpleForm::Inputs::Base
  def input(wrapper_options)
    initial_id = nil
    initial_value = ''

    type = if options.key? :type
             options[:type]
           elsif association_macro == :belongs_to
             :object
           elsif @builder.object.respond_to?(attribute_name)
             :string
           else
             :custom
           end
    
    case type
    when :object
      name ||= "#{@builder.object_name}[#{attribute_name}_id]"
      mode = 'id'
      associated_object = @builder.object.send(attribute_name)
      if associated_object
        initial_id = associated_object.id
        initial_value = label_for_object(associated_object)
      end
    when :string
      mode = 'text'
      initial_value = @builder.object.send(attribute_name)
      name ||= "#{@builder.object_name}[#{attribute_name}]"
    else
      name = options[:name]
      mode = options[:mode]
    end

    if options[:empty]
      initial_id = nil
      initial_value = ''
    end

    template.content_tag(
      'autocomplete-input',
      '',
      name: name,
      mode: mode,
      ':initial-id': initial_id || 'null',
      'initial-value': initial_value,
      ':min-length': options[:min_length] || 3,
      ':allow-freetext': options[:allow_freetext] || false,
      url: options[:autocomplete_url],
      'search-param': options[:search_param] || 'term'
    ).html_safe
  end

  private

  def association_macro
    return unless @builder.object.class.respond_to?(:reflect_on_association)

    reflection = @builder.object.class.reflect_on_association(attribute_name)
    return unless reflection

    reflection.macro
  end

  def label_for_object(obj)
    if obj.respond_to?(:name)
      obj.name
    else
      obj.id
    end
  end
end
