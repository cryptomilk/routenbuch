module ApplicationHelper
  require 'routenbuch/markdown'

  def markdown(text)
    @@markdown ||= Routenbuch::Markdown.new
    @@markdown.render(text.to_s).html_safe
  end

  def alert_class_for flash_type
    case flash_type
      when "success"
        "alert-success"
      when "error"
        "alert-danger"
      when "alert"
        "alert-warning"
      when "notice"
        "alert-info"
      else
        flash_type.to_s
    end
  end

  def thumbnail_tag(image, options = {})
    options[:size] ||= 'thumbnail'
    options[:class] ||= ''

    (max_width, max_height) = case options[:size]
    when :page_preview
      [760, 800]
    when :list_preview
      [640, 320]
    else # :thumbnail
      [150, 150]
    end

    return unless image.attached?

    image_tag(
      image.variant(
        resize_to_limit: [max_width, max_height]
      ),
      srcset: [
        [
          resolve_image_source(
            image.variant(
              resize_to_limit: [(max_width*2), (max_height*2)]
            ),
            true
          ),
          '2x'
        ]
      ],
      class: options[:class],
    )
  end

  def icon_for(target, *additional_class)
    icon = target.respond_to?(:icon) ? target.icon : target
    basic_class = ['fa', "fa-#{icon}"]
    content_tag(
      :i,
      '',
      class: basic_class + additional_class.map { |c| "fa-#{c}" },
      'aria-hidden': true
    )
  end

  def icon_link_to(icon, body, url, options = {})
    icon_class = options[:icon_class] || []
    icon_body = icon_for(icon, *icon_class) + ' ' + body
    link_to(icon_body, url, options)
  end

  def back_button
    return if back_path.nil?

    icon_link_to 'angle-left', _('Back'), back_path, class: 'btn btn-outline-secondary mb-3'
  end
end
