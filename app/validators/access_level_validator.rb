class AccessLevelValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    unless record.class.access_levels.include?(value)
      record.errors[attribute] << (options[:message] || "is not a valid access level")
    end
  end
end
