module SimpleNavigationBootstrap
  class CustomRenderedItem < RenderedItem
    def initialize(renderer, item, level, bootstrap_version)
      super

      if @item.selected?
        @link_options[:class] = 'nav-link active'
      else
        @link_options[:class] = 'nav-link'
      end
    end

    private

    def li_link
      if options.key? :class
        options[:class] += ' nav-item'
      else
        options[:class] = 'nav-item'
      end

      if include_sub_navigation?(item)
        if level == 1
          if split
            splitted_simple_part + splitted_dropdown_part
          else
            content = [item.name]
            content << caret unless skip_caret
            content = content.join(' ').html_safe
            dropdown_part(content)
          end
        else
          content_tag(:li, dropdown_submenu_link, options)
        end
      else
        content_tag(:li, simple_link, options)
      end
    end
  end
end
