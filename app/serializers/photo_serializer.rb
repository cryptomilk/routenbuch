class PhotoSerializer
  include JSONAPI::Serializer

  set_type :photo
  set_id :id
  
  attributes :description

  attribute :url do |object|
    object.photo.url
  end

  attribute :thumb_url do |object|
    object.photo.thumb.url
  end

  attribute :created_at do |object|
    object.created_at.strftime('%Y-%m-%d')
  end
end
