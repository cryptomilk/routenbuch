xml.channel do
  xml.title page_title
  xml.language I18n.locale.to_s
  if @base_object.present?
    description = _('Closures for %{region}') % { region: @base_object.name }
    link = polymorphic_url([@base_object.as_base_class, Closure])
  else
    description = 'All closures'
    link = closures_url
  end
  xml.description description
  xml.link link
  xml.atom :link, href: request.original_url, rel: 'self', type: 'application/rss+xml'

  @closures.each do |closure|
    xml.item do
      xml.title closure.description
      if closure.active?
        xml.description _('Closure is active until %{end_at}.') % { end_at: l(closure.end_at) }
      else
        xml.description _('Closure is no longer active.')
      end
      xml.pubDate closure.active_changed_at.rfc822
      xml.link closure_url(closure)
      xml.guid "#{closure_url(closure)}##{closure.active_changed_at}"
    end
  end
end
