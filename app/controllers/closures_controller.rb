class ClosuresController < ApplicationController
  before_action(
    :set_closure,
    only: %i[
      show edit update destroy
      add_geo_ref_form add_geo_ref remove_geo_ref
      add_route_form add_route remove_route
    ]
  )
  before_action :set_base, only: %i[index feed new new_with_template create]
  before_action :set_add_geo_ref_params, only: [:add_geo_ref_form, :add_geo_ref]
  before_action :set_add_route_params, only: [:add_route_form, :add_route]
  skip_authorization_check only: %i[index browse feed]

  def index
    @q = @base_relation.ransack(params[:q])
    @closures = @q.result \
      .accessible_by(current_ability, :read) \
      .includes(:geo_ref, :geo_refs, :routes) \
      .order('closures.description') \
      .page(params[:page])
    if @base_object.nil?
      @regions = GeoRef.select('DISTINCT ON (geo_refs.id) geo_refs.*') \
        .joins(:closures) \
        .accessible_by(current_ability, :read)
    end
  end

  def feed
    @closures = @base_relation \
      .accessible_by(current_ability, :read) \
      .includes(:geo_ref, :geo_refs, :routes) \
      .where.not(active_changed_at: nil) \
      .reorder(active_changed_at: :desc)
      .page(params[:page])

    respond_to do |format|
      format.html
      format.rss { render layout: 'xml.rss' }
    end
  end

  def show
    authorize! :read, @closure
  end

  def new_with_template
    @templates = ClosureTemplate.all
  end

  def new
    @closure = Closure.new(geo_ref: @base_object)
    authorize! :create, @closure
    template_id = params.dig(:closure_template, :template_id)
    @template = ClosureTemplate.find_by_id(template_id)
    @closure.apply_template(@template) unless @template.nil?
  end

  def edit
    authorize! :update, @closure
  end

  def create
    @closure = Closure.new(closure_params)
    @closure.geo_ref = @base_object
    authorize! :create, @closure
    if @closure.save
      redirect_to @closure, notice: _('Closure was successfully created.')
    else
      render :new
    end
  end

  def update
    authorize! :update, @closure
    if @closure.update(closure_params)
      redirect_to @closure, notice: _('Closure was successfully updated.')
    else
      render :edit
    end
  end

  def destroy
    authorize! :destroy, @closure
    @closure.destroy
    redirect_to closures_url, notice: _('Closure was successfully destroyed.')
  end

  def add_geo_ref_form
    authorize! :update, @closure
  end

  def add_geo_ref
    authorize! :update, @closure
    if @add_geo_ref_params.valid?
      @closure.geo_refs << @add_geo_ref_params.geo_ref
      redirect_to @closure, notice: _('Location has been successfully added.')
    else
      render :add_geo_ref_form
    end
  end

  def remove_geo_ref
    authorize! :update, @closure
    @geo_ref = GeoRef.find(params[:geo_ref_id])
    authorize! :update, @geo_ref
    @closure.geo_refs.delete(@geo_ref)
    redirect_to @closure, notice: _('Location has been successfully removed.')
  end

  def add_route_form
    authorize! :update, @closure
  end

  def add_route
    authorize! :update, @closure
    if @add_route_params.valid?
      @closure.routes << @add_route_params.route
      redirect_to @closure, notice: _('Route has been successfully added.')
    else
      render :add_route_form
    end
  end

  def remove_route
    authorize! :update, @closure
    @route = Route.find(params[:route_id])
    authorize! :update, @route
    @closure.routes.delete(@route)
    redirect_to @closure, notice: _('Route has been successfully removed.')
  end

  private

  def set_base
    @base_object, @base_relation = \
      if params[:geo_ref_id]
        geo_ref = GeoRef.find(params[:geo_ref_id])
        [ geo_ref, geo_ref.self_and_descendent_closures ]
      else
        [ nil, Closure.order('geo_refs.name') ]
      end
    authorize! :read, @base_object unless @base_object.nil?
  end

  def set_closure
    @closure = Closure.find(params[:id])
  end

  def closure_params
    params.require(:closure).permit(
      :description,
      :regular_start_month,
      :regular_start_day_of_month,
      :regular_end_month,
      :regular_end_day_of_month,
      :kind
    )
  end

  def set_add_geo_ref_params
    @add_geo_ref_params = AddGeoRefParams.new
    @add_geo_ref_params.closure = @closure
    geo_ref_id = params.dig(:closures_controller_add_geo_ref_params, :geo_ref_id)
    unless geo_ref_id.blank?
      @add_geo_ref_params.geo_ref = GeoRef.find(geo_ref_id)
      authorize! :update, @add_geo_ref_params.geo_ref
    end
  end

  def set_add_route_params
    @add_route_params = AddRouteParams.new
    @add_route_params.closure = @closure
    route_id = params.dig(:closures_controller_add_route_params, :route_id)
    unless route_id.blank?
      @add_route_params.route = Route.find(route_id)
      authorize! :update, @add_route_params.route
    end
  end
end
