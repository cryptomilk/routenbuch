class Admin::DashboardController < Admin::ApplicationController
  def dashboard
    @geo_ref_count = GeoRef.count
    @route_count = Route.count
    @path_count = Path.count
    @comment_count = Comment.count
    @ascent_count = Ascent.count if Routenbuch.features.ascents?
    @ticklist = Ticklist.count if Routenbuch.features.ticklists?

    @latest_users = User.order(created_at: :desc).limit(10)
  end
end
