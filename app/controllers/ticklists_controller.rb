class TicklistsController < ApplicationController
  before_action :set_ticklist, only: [:edit, :update, :destroy]
  before_action :set_base, only: [:index]
  skip_authorization_check :only => [:index]

  include RequireFeatures
  require_feature :ticklists

  def index
    @q = @base_relation.ransack(params[:q])
    @ticklists = @q.result \
      .includes(route: [:geo_ref, :grade]) \
      .accessible_by(current_ability, :read) \
      .order(created_at: :desc) \
      .page(params[:page])
  end

  def new
    @ticklist = Ticklist.new
    @ticklist.user = current_user
    if params[:route_id].present?
      @ticklist.route = Route.find(params[:route_id])
    end
    authorize! :read, @ticklist
  end

  def edit
    authorize! :update, @ticklist
  end

  def create
    @ticklist = Ticklist.new(ticklist_params)
    @ticklist.user = current_user
    authorize! :create, @ticklist

    if @ticklist.save
      redirect_to geo_ref_path(@ticklist.route.geo_ref), notice: _('Ticklist was successfully created.')
    else
      render :new
    end
  end

  def update
    authorize! :update, @ticklist
    if @ticklist.update(ticklist_params)
      redirect_to user_ticklists_path(current_user), notice: _('Ticklist was successfully updated.')
    else
      render :edit
    end
  end

  def destroy
    authorize! :destroy, @ticklist
    @ticklist.destroy
    redirect_to user_ticklists_path(current_user), notice: _('Ticklist was successfully destroyed.')
  end

  private

  def set_base
    @base_object, @base_relation = \
      if params[:user_id]
        user = User.find(params[:user_id])
        [ user, user.ticklists ]
      elsif params[:route_id]
        route = Route.find(params[:route_id])
        [ route, route.ticklists ]
      elsif params[:geo_ref_id]
        geo_ref = GeoRef.find(params[:geo_ref_id])
        [ geo_ref, geo_ref.ticklists ]
      else
        [ nil, Ticklist ]
      end
    authorize! :read, @base_object unless @base_object.nil?
  end

  def set_ticklist
    @ticklist = Ticklist.find(params[:id])
  end

  def ticklist_params
    params.require(:ticklist).permit(:user_id, :route_id, :comment)
  end
end
