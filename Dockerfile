FROM ruby:2.7
ENV LANG en_US.UTF-8
ENV RAILS_ENV production

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        locales postgresql-client curl nodejs apt-transport-https \
        libvips graphviz gosu \
    && rm -rf /var/lib/apt/lists/*

RUN echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen \
  && locale-gen \
  && /usr/sbin/update-locale en_US.UTF-8

RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
  && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
  && apt-get update \
  && apt-get install -y --no-install-recommends yarn \
  && rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src/app

COPY package.json yarn.lock ./
RUN yarn install

COPY Gemfile* ./
RUN bundle install

COPY . .

EXPOSE 3000

RUN SECRET_KEY_BASE=0 rake assets:precompile

RUN groupadd --system app \
  && useradd --system \
  --comment "Routenbuch application" \
  --create-home \
  -g app \
  app

RUN mkdir -p /data/storage /data/tmp \
  && ln -s /data/storage /usr/src/app/storage \
  && rm -rf /usr/src/app/tmp \
  && ln -s /data/tmp /usr/src/app/tmp

VOLUME [ "/data" ]

ADD docker/entrypoint.sh /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
CMD []

HEALTHCHECK --interval=1m --timeout=60s --retries=5 \
  CMD curl -f http://localhost:3000/health_check || exit 1

