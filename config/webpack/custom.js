const webpack = require('webpack')

module.exports = {
  module: {
    rules: [
      {
        test: require.resolve('jquery'),
        use: [{
          loader: 'expose-loader',
          options: 'jQuery'
        },{
          loader: 'expose-loader',
          options: '$'
        }]
      },
      {
        test: require.resolve('leaflet'),
        use: [{
          loader: 'expose-loader',
          options: 'L'
        },{
          loader: 'expose-loader',
          options: 'window.L'
        }]
      }
    ]
  },
  plugins: [
    new webpack.ProvidePlugin({
      L: 'leaflet',
      'window.L': 'leaflet'
    })
  ],
  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js'
    }
  }
}
