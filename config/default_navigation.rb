# -*- coding: utf-8 -*-
# Configures your navigation

require 'simple_navigation_bootstrap/custom_bootstrap'

SimpleNavigation::Configuration.run do |navigation|
  navigation.renderer = SimpleNavigationBootstrap::CustomBootstrap

  navigation.items do |primary|
    primary.item(
      :ascents,
      {
        icon: Ascent.icon,
        text: _('Ascents')
      },
      ascents_path,
      highlights_on: %r(^/ascents)
    ) if Routenbuch.features.ascents?

    primary.item(
      :comments,
      {
        icon: Comment.icon,
        text: _('Comments')
      },
      comments_path,
      highlights_on: %r(^/comments)
    )
    primary.item(
      :photos,
      {
        icon: Photo.icon,
        text: _('Photos')
      },
      photos_path,
      highlights_on: %r(^/photos)
    )
    primary.item(
      :geo_refs,
      {
        icon: GeoRef.icon,
        text: _('Regions')
      }, browse_geo_refs_path,
      highlights_on: %r(^/geo_refs)
    )
    primary.item(
      :paths,
      {
        icon: Path.icon,
        text: _('Paths')
      }, paths_path,
      highlights_on: %r(^/paths)
    )
    primary.item :routes, { icon: Route.icon, text: _('Routes') }, routes_path,
      highlights_on: %r(^/routes)
    primary.item :closures, { icon: Closure.icon, text: _('Closures') }, closures_path
    primary.item :users, { icon: User.icon, text: _('Users') }, users_path
    primary.item :scales, { icon: Grade.icon, text: _('Grades') }, '/scales/free_climbing'
    primary.item :first_ascent_people, { icon: FirstAscentPerson.icon, text: _('First Ascensionists') }, first_ascent_people_path

    primary.item(
      :regulations,
      {
        icon: Regulation.icon,
        text: _('Climbing regulations')
      }, regulations_path,
      highlights_on: %r(^/regulations)
    )

    primary.item(
      :admin,
      {
        icon: 'cog',
        text: _('Admin panel')
      },
      admin_root_path,
    ) if can? :access, :admin
  end
end
