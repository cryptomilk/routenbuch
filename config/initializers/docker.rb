# autoflush stdout when running in container
if File.exists?("/.dockerenv")
  $stdout.sync = true
end
