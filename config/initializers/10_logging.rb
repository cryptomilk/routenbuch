# custom logging is only used on production
# for development stay with default rails logging
if Rails.env.production?
  require 'routenbuch/json_formatter'

  Rails.logger.level = Routenbuch.log_level

  Rails.application.configure do
    config.lograge.enabled = true

    if Routenbuch.log_format == :json
      config.logger.formatter = Routenbuch::JsonFormatter.new
      config.lograge.formatter = Lograge::Formatters::Raw.new
    end

    if Routenbuch.log_user
      config.lograge.custom_payload do |controller|
        {
          host: controller.request.host,
          user: controller.current_user.try(:email)
        }
      end
    else
      config.lograge.custom_payload do |controller|
        {
          host: controller.request.host,
        }
      end
    end

    config.lograge.custom_options = lambda do |event|
      options = {}
      if (e = event.payload[:exception_object])
        options[:exception] = {
          class: e.class.name,
          message: e.message,
          backtrace: e.backtrace
        }
      end
      options
    end
  end
end
