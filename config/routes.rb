Rails.application.routes.draw do
  require 'sidekiq/web'
  require 'sidekiq/cron/web'
  authenticate :user, lambda { |u| u.role == 'admin' } do
      mount Sidekiq::Web => '/sidekiq'
  end
  devise_for :users

  get '403', to: 'errors#forbidden', as: 'forbidden'
  get '404', to: 'errors#not_found', as: 'not_found'
  get '422', to: 'errors#unprocessable_entity'
  get '500', to: 'errors#internal_server_error'

  root to: "geo_refs#index"

  get 'profile', to: 'profile#show'
  patch 'profile', to: 'profile#update'
  get 'profile/password', to: 'profile#password'
  patch 'profile/password', to: 'profile#update_password'

  resources :search, only: %i[index]
  resources :photos, only: %i[index show edit update destroy]
  resources :topos
  resources :zones, only: %i[index]
  resources :regulations, only: %i[index show edit update destroy] do
    resources :regulated_geo_refs, only: %i[new create]
  end
  resources :regulated_geo_refs, only: %i[edit update destroy]
  resources :routes, only: %i[index show edit update destroy] do
    collection do
      get 'autocomplete'
    end
    resources :ascents, only: [:index, :new]
    resources :ticklists, only: [:index, :new]
    resources :photos, only: %i[index new create]
    resources :comments, only: %i[index new create]
    resources :inventories do
      member do
        put 'remove'
      end
    end
    resources :route_links, only: %i[new create edit update destroy]
  end

  resources :closures, only: %i[index show edit update destroy] do
    member do
      get :add_geo_ref_form
      post :add_geo_ref
      post :remove_geo_ref
      get :add_route_form
      post :add_route
      post :remove_route
    end
    collection do
      get :feed
    end
    resources :season_closures, only: %i[new create]
  end
  resources :season_closures, only: %i[edit update destroy]

  resources :ascents, only: %i[index show]
  resources :ticklists, only: %i[index show]

  resources :users, only: %i[show index] do
    resources :ascents, except: :show
    resources :ticklists, except: :show
    resources :photos, only: [:index]
  end

  resources :first_ascent_people do
    collection do
      get 'autocomplete'
    end
  end
  get 'scales/:scope', to: 'scales#table', defaults: { scope: 'climbing' }

  resources :geo_refs, except: :new  do
    collection do
      get(
        'new/:type',
        to: 'geo_refs#new',
        constraints: { type: /(Country|Region|Crag|Sector|Parking)/ },
        as: 'new'
      )
      get 'browse'
      get 'autocomplete'
      get 'map'
    end

    member do
      get 'map_items'
      get :sort_routes
      put :reorder_routes
      get :sort_childs
      put :reorder_childs
      get :select_items
      patch :move_items
    end

    resources :routes, only: %i[new create index] do
      collection do
        get :autocomplete
      end
    end
    resources :ascents, only: [:index]
    resources :ticklists, only: [:index]
    resources :approaches, only: [:index]
    resources :photos, only: %i[index new create]
    resources :topos
    resources :secondary_zones, only: %i[new create edit update destroy]
    resources :comments, only: %i[index new create]
    resources :paths, only: %i[index new create]
    resources :permissions, only: %i[index create edit update destroy]
    resources :regulations, only: %i[index new create]
    resources :closures, only: %i[index new create] do
      collection do
        get :new_with_template
        get :feed
      end
    end
  end

  resources :grades, only: [] do
    collection do
      get 'autocomplete'
    end
  end

  resources :styles, only: [] do
    collection do
      get 'autocomplete'
    end
  end

  resources :comments, only: %i[index edit update destroy]

  resources :paths, only: %i[index show edit update destroy] do
    member do
      post 'add_geo_ref'
      post 'remove_geo_ref'
      get 'map_items'
    end
  end

  resources :products, only: [] do
    collection do
      get 'autocomplete'
    end
  end

  get 'help', to: redirect('/help/README.html'), as: nil
  get 'help/*path', to: 'help#show', as: 'help'
  
  namespace :admin do
    root to: 'dashboard#dashboard'
    get '/monitoring', to: 'monitoring#index'
    get '/sys_info', to: 'sys_info#index'
    get '/background_jobs', to: 'background_jobs#index'
    resources :users do
      collection do
        get :members
        get :contributors
        get :admins
        get 'autocomplete'
      end
    end

    resources :vendors do
      collection do
        get 'autocomplete'
      end
    end
    resources :products do
      collection do
        get 'autocomplete'
      end
    end
  end

  get 'custom_layout', format: :css, to: 'custom_layout#index', as: :custom_layout

  get 'password_strength', to: 'password_strength#check'

  get '/abilities/:id', to: 'abilities#show'

  health_check_routes
end
