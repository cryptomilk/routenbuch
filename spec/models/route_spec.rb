require 'rails_helper'

RSpec.describe Route, type: :model do
  let(:attributes) { {} }
  let(:route) { create :route, **attributes }

  context 'route' do
    it 'validation' do
      expect { route.validate! }.not_to raise_error
    end
  end
end
