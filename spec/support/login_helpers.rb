module LoginHelpers
  def login_user(user_factory)
    before(:each) do
      @request.env["devise.mapping"] = Devise.mappings[user_factory] unless @request.nil?
      user = FactoryBot.create(user_factory)
      sign_in user
    end
  end
end
