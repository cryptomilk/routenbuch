FactoryBot.define do
  factory :zone do
    name { 'Zone X' }
    body { 'climbing allowed' }
    organization { 'Climbing organization' }
    icon_path { 'dummy.svg' }
  end
end
