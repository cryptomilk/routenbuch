require 'rails_helper'

RSpec.describe GeoRefsController, type: :request do
  login_user :admin_user

  before(:each) do
    @crag = create(:crag)
  end

  it 'index' do
    get geo_refs_path
    expect(response).to have_http_status(:ok)
  end

  it 'show' do
    get geo_ref_path(@crag)
    expect(response).to have_http_status(:ok)
  end

  it 'edit' do
    get edit_geo_ref_path(@crag)
    expect(response).to have_http_status(:ok)
  end

  it 'update' do
    patch(
      geo_ref_path(@crag),
      params: {
        geo_ref: {
          description: 'update me!'
        }
      }
    )
    expect(response).to have_http_status(:found)
    @crag.reload
    expect(@crag.description).to eq('update me!')
  end

  it 'destroy' do
    delete geo_ref_path(@crag)
    expect(response).to have_http_status(:found)
    expect(GeoRef.exists? @crag.id).to be false
  end
end
