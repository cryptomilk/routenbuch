require 'rails_helper'

RSpec.describe ClosuresController, type: :request do
  login_user :admin_user

  before(:each) do
    @closure = create(:closure)
  end

  it 'index' do
    get closures_path
    expect(response).to have_http_status(:ok)
  end

  it 'index on geo_ref' do
    get geo_ref_closures_path(@closure.geo_ref)
    expect(response).to have_http_status(:ok)
  end

  it 'feed' do
    get feed_closures_path
    expect(response).to have_http_status(:ok)
  end

  it 'feed on geo_ref' do
    get feed_geo_ref_closures_path(@closure.geo_ref)
    expect(response).to have_http_status(:ok)
  end

  it 'new' do
    get new_geo_ref_closure_path(create(:crag))
    expect(response).to have_http_status(:ok)
  end

  it 'create' do
    post(
      geo_ref_closures_path(create(:crag)),
      params: {
        closure: {
          description: 'my closure'
        }
      }
    )
    expect(response).to have_http_status(:found)
    follow_redirect!
    expect(response.body).to include("Closure was successfully created.")
  end

  it 'show' do
    get closure_path(@closure)
    expect(response).to have_http_status(:ok)
  end

  it 'edit' do
    get edit_closure_path(@closure)
    expect(response).to have_http_status(:ok)
  end

  it 'update' do
    patch(
      closure_path(@closure),
      params: {
        closure: {
          description: 'update me!'
        }
      }
    )
    expect(response).to have_http_status(:found)
    @closure.reload
    expect(@closure.description).to eq('update me!')
    follow_redirect!
    expect(response.body).to include("Closure was successfully updated.")
  end

  it 'destroy' do
    delete closure_path(@closure)
    expect(response).to have_http_status(:found)
    follow_redirect!
    expect(response.body).to include("Closure was successfully destroyed.")
    expect(Closure.exists? @closure.id).to be false
  end

  it 'add_geo_ref_form' do
    get add_geo_ref_form_closure_path(@closure)
    expect(response).to have_http_status(:ok)
  end

  it 'add_geo_ref' do
    geo_ref = create(:crag)
    post(
      add_geo_ref_closure_path(@closure),
      params: {
        closures_controller_add_geo_ref_params: {
          geo_ref_id: geo_ref.id
        }
      }
    )
    expect(response).to have_http_status(:found)
    follow_redirect!
    expect(response.body).to include('Location has been successfully added.')
    expect(@closure.geo_ref_ids).to include geo_ref.id
  end

  it 'remove_geo_ref' do
    geo_ref = create(:crag)
    @closure.geo_refs << geo_ref
    post(
      remove_geo_ref_closure_path(@closure),
      params: {
        geo_ref_id: geo_ref.id
      }
    )
    expect(response).to have_http_status(:found)
    follow_redirect!
    expect(response.body).to include('Location has been successfully removed.')
    @closure.reload
    expect(@closure.geo_ref_ids).not_to include geo_ref.id
  end

  it 'add_route_form' do
    get add_route_form_closure_path(@closure)
    expect(response).to have_http_status(:ok)
  end

  it 'add_route' do
    route = create(:route)
    post(
      add_route_closure_path(@closure),
      params: {
        closures_controller_add_route_params: {
          route_id: route.id
        }
      }
    )
    expect(response).to have_http_status(:found)
    follow_redirect!
    expect(response.body).to include('Route has been successfully added.')
    expect(@closure.route_ids).to include route.id
  end

  it 'remove_route' do
    route = create(:route)
    @closure.routes << route
    post(
      remove_route_closure_path(@closure),
      params: {
        route_id: route.id
      }
    )
    expect(response).to have_http_status(:found)
    follow_redirect!
    expect(response.body).to include('Route has been successfully removed.')
    @closure.reload
    expect(@closure.route_ids).not_to include route.id
  end
end
