
require 'redcarpet'

module Routenbuch
  class Markdown

    def initialize
      render_options = {
        filter_html: true,
        no_images:   true,
        no_links:    true,
        no_styles:   true,
      }
      extensions = {
        tables: true,
        strikethrough: true,
        underline: true,
        highlight: true,
        quote: true,
      }
      renderer = Redcarpet::Render::HTML.new(render_options)
      @parser = Redcarpet::Markdown.new(renderer, extensions)
    end

    def render(text)
      @parser.render(text)
    end
  end
end 
