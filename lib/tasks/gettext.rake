namespace :gettext do
  def files_to_translate
    Dir.glob("{app,lib,config,locale}/**/*.{rb,erb,haml,slim}") \
      + Dir.glob("db/seeds/*.rb")
  end
end
