#!/bin/bash

set -e
pid=0

# SIGTERM-handler
term_handler() {
  if [ $pid -ne 0 ]; then
    kill -SIGTERM "$pid"
    wait "$pid"
  fi
  exit 143;
}
trap term_handler SIGTERM

ensure_permissions() {
  while (($#)); do
    test -e "$1" || mkdir -p "$1"
    chown app:app "$1"
    shift
  done
}

ensure_permissions \
  /usr/src/app/public/packs \
  /usr/src/app/public/packs-test \
  /usr/src/app/tmp/cache \
  /usr/src/app/tmp/pids \
  /usr/src/app/storage \

rm -rf /data/run/*.pid

if [ "$CI" == "true" ] ; then
  gosu app /bin/bash
elif [ $# -eq 0 ] ; then
  gosu app bundle exec rails server -b 0.0.0.0 &
  pid="$!"
  wait "$pid"
else
  gosu app $@
fi

exit 0

