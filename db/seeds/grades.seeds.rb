
free_scales = [ 'USA', 'French', 'UIAA', 'Fontainebleau' ]

free_grades = [
  [   '5.2',   '1',   '1',   nil ],
  [   '5.3',   '2',   '2',   nil ],
  [   '5.4',   '3',   '3',   '2' ],
  [   '5.5',   '4',   '4',   '3' ],
  [   '5.6',  '5a',  '5-',   nil ],
  [   '5.7',   nil,   '5',  '4a' ],
  [     nil,  '5b',  '5+',   nil ],
  [   '5.8',   nil,  '6-',   nil ],
  [   '5.9',  '5c',   '6',  '4b' ],
  [ '5.10a',  '6a',  '6+',   nil ],
  [ '5.10b', '6a+',  '7-',   nil ],
  [ '5.10c',  '6b',   '7',   nil ],
  [ '5.10d', '6b+',  '7+',  '4c' ],
  [ '5.11a',  '6c','7+/8-',  '5a' ],
  [ '5.11b', '6c+',  '8-',   nil ],
  [ '5.11c',  '7a',   '8',  '5b' ],
  [ '5.11d', '7a+',  '8+',  '5c' ],
  [ '5.12a',  '7b','8+/9-',  '6a' ],
  [ '5.12b', '7b+',  '9-',  '6b' ],
  [ '5.12c',  '7c',   '9',  '6c' ],
  [ '5.12d', '7c+',  '9+',  '7a' ],
  [ '5.13a',  '8a','9+/10-',  '7a+' ],
  [ '5.13b',   nil,   nil,  '7b' ],
  [ '5.13c', '8a+', '10-',  '7b+' ],
  [     nil,   nil,   nil,  '7c' ],
  [ '5.13d',  '8b',  '10',  '7c+' ],
  [ '5.14a', '8b+', '10+',  '8a' ],
  [ '5.14b',  '8c','10+/11-',  '8a+' ],
  [ '5.14c', '8c+', '11-',  '8b' ],
  [ '5.14d',  '9a',  '11',  '8b+' ],
  [     nil,   nil,   nil,  '8c' ],
  [ '5.15a', '9a+','11/11+',  '8c+' ],
  [     nil,   nil, '11+',    nil ],
  [ '5.15b',  '9b','11+/12-',    nil ],
  [     nil, '9b+',   '12-',    nil ],
  [     nil, '9c',    '12',    nil ],
]

aid_scales = [ 'traditional', 'clean' ]

aid_grades = [
  [ 'A0', 'C0'],
  [ 'A1', 'C1'],
  [ 'A2', 'C2'],
  [  nil, 'C2+'],
  [ 'A3', 'C3'],
  [  nil, 'C3+'],
  [ 'A4', 'C4'],
  [  nil, 'C4+'],
  [ 'A5', 'C5'],
]

risk_scales = [ 'Ernsthaftigkeit' ]

risk_grades = [
  [ 'E1'],
  [ 'E1-'],
  [ 'E2+'],
  [ 'E2'],
  [ 'E2-'],
  [ 'E3+'],
  [ 'E3'],
  [ 'E3-'],
  [ 'E4+'],
  [ 'E4'],
  [ 'E4-'],
  [ 'E5+'],
  [ 'E5'],
  [ 'E5-'],
  [ 'E6+'],
  [ 'E6'],
  [ 'E6-'],
]

categories = [
  { name: 'free_climbing', scales: free_scales, grades: free_grades },
  { name: 'aid_climbing', scales: aid_scales, grades: aid_grades },
  { name: 'risk', scales: risk_scales, grades: risk_grades },
]

def category_for(difficulty)
  case difficulty
  when 0..5 then N_('very-easy')
  when 6..9 then N_('easy')
  when 10..12 then N_('medium')
  when 13..16 then N_('hard')
  when 17..20 then N_('extrem')
  else
    N_('ultra')
  end
end

# recreate them
categories.each do |category|
  category[:grades].each_with_index do |grades_row,difficulty|
    category[:scales].each_with_index do |scale,grade_column|
      next if grades_row[grade_column].nil?
      Grade.find_or_create_by!(
        scale: scale,
        scope: category[:name],
        grade: grades_row[grade_column],
        difficulty: difficulty,
        category: category_for(difficulty)
      )
    end
  end
end

