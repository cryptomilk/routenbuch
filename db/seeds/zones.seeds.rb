ig_name = 'IG-Klettern Frankenjura & Fichtelgebirge e.V.'

zones = {
  'Zone 1' => {
    body: 'Ruhezone, hier wird grundsätzlich nicht geklettert',
    organization: ig_name,
    icon_path: 'icons/zones/zone1.svg'
  },
  'Zone 2' => {
    body: 'Klettern nur auf den vorhandenen Routen bis zum Umlenkhaken, keine Neutouren (Neutouren evtl. nach Rücksprache mit der zuständigen Behörde möglich)',
    organization: ig_name,
    icon_path: 'icons/zones/zone2.svg'
  },
  'Zone 3' => {
    body: 'Klettern auf den vorhandenen Routen, außerhalb von Vegetationszonen sind Neutouren mit Umlenkhaken möglich',
    organization: ig_name,
    icon_path: 'icons/zones/zone3.svg'
  },
  'Zone 0' => {
    body: 'Nicht genehmigter Fels',
    organization: ig_name,
    icon_path: 'icons/zones/zone0.svg'
  }
}

zones.each do |name, data|
  zone = Zone.find_by(name: name)
  if zone.present?
    zone.update!(**data)
  else
    Zone.create!(
      name: name,
      **data
    )
  end
end
