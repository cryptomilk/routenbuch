[
  [N_('Peregrine falcon'), N_('Bird protection [...]'), 2, 1, 6, 30],
  [N_('Eagle Owl'), N_('Bird protection [...]'), 1, 1, 7, 31],
  [N_('Bat'), N_('Bat protection [...]'), 10, 1, 3, 31],
].each do |name, description, start_month, start_day, end_month, end_day|
  ClosureTemplate.find_or_create_by!(
    name: name
  ) do |c|
    c.description = description
    c.regular_start_month = start_month
    c.regular_start_day_of_month = start_day
    c.regular_end_month = end_month
    c.regular_end_day_of_month = end_day
  end
end
