class RevertJsonbToJson < ActiveRecord::Migration[6.1]
  def change
    reversible do |dir|
      dir.up do
        change_column :geo_refs, :stats, 'json USING CAST(stats AS json)'
        change_column :users, :stats, 'json USING CAST(stats AS json)'
      end

      dir.down do
        change_column :geo_refs, :stats, 'jsonb USING CAST(stats AS jsonb)'
        change_column :users, :stats, 'jsonb USING CAST(stats AS jsonb)'
      end
    end
  end
end
