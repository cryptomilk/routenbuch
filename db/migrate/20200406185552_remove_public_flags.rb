class RemovePublicFlags < ActiveRecord::Migration[6.0]
  def change
    remove_column :geo_refs, :public, :boolean, default: true, null: false
    remove_column :tags, :public, :boolean
  end
end
