class DropGeoRefTypeTable < ActiveRecord::Migration[6.0]
  def change
    drop_table :geo_ref_types do |t|
      t.string :name
      t.boolean :show_childs
      t.boolean :show_routes
      t.timestamps
    end
  end
end
